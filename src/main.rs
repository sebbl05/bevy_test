use bevy::prelude::*;

#[bevy_main]
fn main() {
    App::build()
        //        .insert_resource(Msaa { samples: 4 })
        .insert_resource(WindowDescriptor {
            title: "Juhu!".to_string(),
            width: 1280.0,
            height: 720.0,
            ..Default::default()
        })
        .add_plugins(DefaultPlugins)
        .run();
}
